#!/bin/bash

# Manage disk and partitions
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

clear

echo -e "1-format.sh Started\n"

disk="$1"
root_part="$2"
efi_part="$3"
crypto_password="$4"
format_disk="$5"
disk_performance="$6"

# Detect EFI presence
[ -d /sys/firmware/efi ] && efi="true"

# Detect SSD presence
disk_without_dev="$(echo -e $disk | sed 's/\/dev\///g')"
[ "$(cat /sys/block/$disk_without_dev/queue/rotational)" == 0 ] && ssd="true"

# Create partitions. This is following the partition scheme 
# of the "define partitions if not supplied" on 0-aais.sh
[ "$format_disk" == "true" ] && if [ "$efi" == "true" ] ; then
	# EFI boot devices
	echo -e "Yes" | parted "$disk" mklabel gpt ---pretend-input-tty
	parted "$disk" mkpart EFI fat32 1MiB 261MiB
	parted "$disk" mkpart ROOT btrfs 261MiB 100%
	parted "$disk" set 1 esp on
else
	# Legacy boot devices
	echo -e "Yes" | parted "$disk" mklabel msdos ---pretend-input-tty
	parted "$disk" mkpart primary btrfs 1MiB 100%
	parted "$disk" set 1 boot on
fi

# Make sure that systemd knows the changes
systemctl daemon-reload

# Encrypt: This can change root_part to /dev/mapper/root
[ "$crypto_password" != "false" ] && {
	echo -e "$crypto_password" | cryptsetup luksFormat --type luks1 --iter-time 1000 "$root_part"
	echo -e "$crypto_password" | cryptsetup luksOpen "$root_part" root
	root_part="/dev/mapper/root"
}

# Format partitions
[[ "$efi" == "true" && "$format_disk" == "true" ]] && mkfs.fat -n EFI -F32 -S 4096 "$efi_part"
mkfs.btrfs -L ROOT -f "$root_part"

# Create btrfs subvolumes
mount "$root_part" /mnt 
btrfs subvolume create /mnt/@
mkdir /mnt/@/home
btrfs subvolume create /mnt/@home

[ "$efi" == "true" ] && mkdir -p /mnt/@/boot/efi
umount /mnt

# Setup best mount arguments
if [ "$disk_performance" == "y" ] ; then
	mount_arguments="defaults,compress-force=zstd:3"
else
	mount_arguments="defaults,compress-force=zstd:1"
fi

# Use discard=async if device is a SSD
[ "$ssd" == "true" ] && mount_arguments="$mount_arguments,discard=async"

# Mount partitions and subvolumes
mount -o "$mount_arguments",subvol=@ "$root_part" /mnt 
mount -o "$mount_arguments",subvol=@home "$root_part" /mnt/home
[ "$efi" == "true" ] && mount -o rw,defaults "$efi_part" /mnt/boot/efi

clear
