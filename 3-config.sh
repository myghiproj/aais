#!/bin/bash

# Configure things for both server and desktop modes
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

echo -e "3-config Started\n"

user="$1"
password="$2"
hostname="$3"
installation_mode="$4"
disk="$5"
root_part="$6"
language="$7"
keymap="$8"
region="$9"
crypto_password="${10}"
format_disk="${11}"
disk_performance="${12}"

# Detect EFI presence
[ -d /sys/firmware/efi ] && efi="true"

# Detect SSD presence
disk_without_dev="$(echo -e $disk | sed 's/\/dev\///g')"
[ "$(cat /sys/block/$disk_without_dev/queue/rotational)" == 0 ] && ssd="true"

# Cleanup pacman cache
rm -rf /var/cache/pacman/pkg/*

# Edit mkinitcpio.conf
# Remove fsck (not neccessary with btrfs) + add AMD and Intel graphics modules to avoid problems + IDE, AHCI and USB modules to boot
sed -i -e '/HOOKS\=(base\ udev\ autodetect\ modconf/s/\ fsck//g' -e '/HOOKS\=(base\ udev\ autodetect\ modconf/s/\ consolefont//g' -e '/MODULES_DECOMPRESS=/s/#//g' /etc/mkinitcpio.conf

# Initial RamDisk encryption setup
[ "$crypto_password" != "false" ] && {
	# Add needed kernel modules at boot
	sed -i '/HOOKS\=(base\ udev\ autodetect\ modconf/s/block/block\ encrypt\ lvm2/' /etc/mkinitcpio.conf

	# Create Extra keyfile to avoid typing the key twice and add to mkinitcpio.conf
	dd bs=512 count=4 if=/dev/random of=/crypto_keyfile.bin iflag=fullblock
	chmod 0400 /crypto_keyfile.bin
	echo -e "$crypto_password" | cryptsetup -v luksAddKey --iter-time 100 --pbkdf pbkdf2 "$root_part" /crypto_keyfile.bin
	sed -i '/FILES=(/s/(.*/(\/crypto_keyfile\.bin)/g' /etc/mkinitcpio.conf
}

# Generate RamDisk
if [ "$crypto_password" != "false" ] ; then
	# Generate and adjust permissions
	mkinitcpio -P
	chmod 2600 /boot/initramfs-linux*
else
	# Only run mkinitcpio on server mode to avoid running it twice
	[ "$installation_mode" == "server" ] && mkinitcpio -P
fi

# Configure grub kernel arguments
if [ "$crypto_password" == "false" ] ; then
	# Non-encrypted disks
	sed -i -e '/AL_OUT/s/^#//' -e '/LINUX_DEFAULT/s/w*\".*\"/\"loglevel=3\ acpi_osi=Linux\ nowatchdog\ zswap.enabled=0\"/' /etc/default/grub
else
	# Set up encryption
	cryptuuid="$(blkid -s UUID -o value $root_part)"

	if [ "$ssd" == "true" ] ; then
		sed -i -e '/GRUB_ENABLE_CRYPTODISK/s/#//g' -e '/AL_OUT/s/^#//' -e '/LINUX_DEFAULT/s/w*\".*\"/\"loglevel=3\ acpi_osi=Linux\ nowatchdog\ zswap.enabled=0\ cryptdevice=UUID='$cryptuuid':root:allow-discards\ root=\/dev\/mapper\/root\"/' /etc/default/grub
	else
		sed -i -e '/GRUB_ENABLE_CRYPTODISK/s/#//g' -e '/AL_OUT/s/^#//' -e '/LINUX_DEFAULT/s/w*\".*\"/\"loglevel=3\ acpi_osi=Linux\ nowatchdog\ zswap.enabled=0\ cryptdevice=UUID='$cryptuuid':root\ root=\/dev\/mapper\/root\"/' /etc/default/grub
	fi
fi

if [ "$format_disk" == "true" ] ; then 
	# If arch is gonna be the only OS, decrease the GRUB timeout to 1 second
	sed -i '/TIMEOUT=/s/\w*$/1/' /etc/default/grub
else 
	# Enable OS Prober if it's gonna multi-boot
	sed -i -e '/#GRUB_DISABLE_OS_PROBER/s/#//g' /etc/default/grub
fi

# Install grub
if [ "$efi" == "true" ] ; then
	# Install efi boot entry on the same place as windows if arch it's the only OS
	[ "$format_disk" == "true" ] && grub_args="--removable"
	grub_args="$grub_args --efi-directory=/boot/efi --bootloader-id='Arch Linux'"
	grub-install $grub_args
else
	grub-install "$disk"
fi

grub-mkconfig -o /boot/grub/grub.cfg

# Very common settings
echo -e "$password\n$password" | passwd
useradd -m "$user"
echo -e "$password\n$password" | passwd "$user"
gpasswd -a "$user" wheel
echo "$user ALL=(ALL) ALL" >> /etc/sudoers
sed -i -e '/#en_US.UTF/s/#//g' -e '/#'$language'/s/#//g' /etc/locale.gen
locale-gen
ln -sf /usr/share/zoneinfo/"$region" /etc/localtime || :
hwclock --systohc
echo -e "$hostname" > /etc/hostname
echo -e "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.0.1\t$hostname.localdomain\t$hostname" >> /etc/hosts
echo -e "LANG=$language" > /etc/locale.conf
echo -e "KEYMAP=$keymap" > /etc/vconsole.conf
sed -i '/Parall/s/^#//' /etc/pacman.conf

# Limit log max size
sed -i -e 's/#SystemMaxUse=/SystemMaxUse=25M/' -e 's/#SystemMaxFileSize=/SystemMaxFileSize=25M/' -e 's/#RuntimeMaxUse=/RuntimeMaxUse=25M/' -e 's/#RuntimeMaxFileSize=/RuntimeMaxFileSize=25M/' /etc/systemd/journald.conf

# Set the correct disk scheduler depending on the type of the disk
echo -e "#NVME's\nACTION==\"add|change\", KERNEL==\"nvme[0-9]n[0-9]\", ATTR{queue/scheduler}=\"none\"\n\n# SSD's SATA e e-MMC's\nACTION==\"add|change\", KERNEL==\"sd[a-z]|mmcblk[0-9]*\", ATTR{queue/rotational}==\"0\", ATTR{queue/scheduler}=\"mq-deadline\"\n\n# HD's\nACTION==\"add|change\", KERNEL==\"sd[a-z]\", ATTR{queue/rotational}==\"1\", ATTR{queue/scheduler}=\"bfq\"" > /etc/udev/rules.d/60-ioschedulers.rules

# Optimize power management depending on the type of the disk
echo -e "ACTION==\"add|change\", KERNEL==\"sd[a-z]\", ATTRS{queue/rotational}==\"1\", RUN+=\"/usr/bin/hdparm -B 128 -S 240 /dev/%k\"" > /etc/udev/rules.d/69-hdparm.rules

# Configure SWAP behaviour to use with zram + enable performance support for intel graphics
echo -e "# INTEL GRAPHICS WORKAROUND\ndev.i915.perf_stream_paranoid=0\n\n# MEMORY MANAGEMENT\nvm.dirty_ratio=10\nvm.vfs_cache_pressure=50\nvm.dirty_background_ratio=5\nvm.swappiness=200\nvm.page-cluster=0" > /etc/sysctl.d/99-sysctl.conf

# Configure Zram (improves performance A LOT on swap usage)
echo -e "[zram0]\nzram-size = min(min(ram, 4096) + max(ram - 4096, 0) / 2, 8192)\ncompression-algorithm = lz4\nswap-priority = 32767" > /etc/systemd/zram-generator.conf

# Manage systemd services
systemctl mask systemd-fsck-root.service systemd-rfkill.service systemd-rfkill.socket hibernate.target hybrid-sleep.target
systemctl enable power-profiles-daemon irqbalance earlyoom systemd-timesyncd cronie thermald upower

clear
