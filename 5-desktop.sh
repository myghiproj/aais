#!/bin/bash

# Install and configure specific things for desktop mode
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

echo -e "5-desktop.sh Started\n"

user="$1"
keyboard_layout="$2"
keyboard_variant="$3"
audio_quality="$4"
autologin="$5"
desktop_environment="$6"

# X.org, vulkan, opengl and video acceleration drivers, audio, virtual filesystems, archive plugins, printer and some libraries
packages_base="xorg-server xorg-xkill xf86-input-wacom vulkan-radeon lib32-vulkan-radeon vulkan-intel lib32-vulkan-intel mesa lib32-mesa mesa-utils lib32-mesa-utils libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau libva-intel-driver lib32-libva-intel-driver intel-media-driver libva-utils pipewire lib32-pipewire pipewire-alsa pipewire-pulse pipewire-jack lib32-pipewire-jack wireplumber alsa-firmware sof-firmware alsa-ucm-conf gvfs gvfs-mtp gvfs-afc gvfs-gphoto2 gvfs-nfs gvfs-smb p7zip unrar lrzip squashfs-tools unarchiver cups nss-mdns libraw exiv2"



# XFCE related packages
packages_xfce="exo garcon thunar thunar-volman tumbler xfce4-panel xfce4-power-manager xfce4-session xfce4-settings xfce4-terminal xfconf xfdesktop xfwm4 thunar-archive-plugin thunar-media-tags-plugin xfce4-battery-plugin xfce4-notifyd xfce4-pulseaudio-plugin xfce4-screenshooter xfce4-whiskermenu-plugin catfish"

# Additional packages to improve XFCE appearance, integrations and features 
packages_xfce_extra="gnome-keyring xdg-user-dirs xdg-desktop-portal xdg-desktop-portal-gtk webp-pixbuf-loader ffmpegthumbnailer noto-fonts noto-fonts-cjk noto-fonts-emoji xfce4-screensaver lightdm-gtk-greeter-settings blueman network-manager-applet alacarte pavucontrol system-config-printer gtk-engine-murrine kvantum arc-gtk-theme papirus-icon-theme breeze-icons"



# Cinnamon related packages
packages_cinnamon="cinnamon gnome-terminal blueberry metacity lightdm lightdm-gtk-greeter"

# Additional packages to improve Cinnamon appearance, integrations and features
packages_cinnamon_extra="xdg-user-dirs xdg-desktop-portal xdg-desktop-portal-gtk webp-pixbuf-loader ffmpegthumbnailer noto-fonts noto-fonts-cjk noto-fonts-emoji lightdm-gtk-greeter-settings blueman network-manager-applet alacarte pavucontrol system-config-printer gtk-engine-murrine kvantum arc-gtk-theme papirus-icon-theme breeze-icons"



# Extra userspace applications
packages_userspace="firefox audacious mpv yt-dlp evince gthumb mousepad gnome-system-monitor gnome-disk-utility mate-calc ark gufw seahorse"


# Enable Multilib Repository
sed -i -e 's/#\[multilib]/\[multilib]/' -e '/\[multilib]/{n;s/^#//}' /etc/pacman.conf


packages_de="$packages_xfce"
packages_de_extra="$packages_xfce_extra"

# Install packages loop
if [ "$desktop_environment" == "cinnamon" ] ; then
	echo -e "\nInstalling Cinnamon..."
	packages_de="$packages_cinnamon"
	packages_de_extra="$packages_cinnamon_extra"
else
	echo -e "\nInstalling XFCE..."
fi

for packages in "$packages_base" "$packages_de" "$packages_de_extra" "$packages_userspace" ; do
	args="--needed --noconfirm"
	installation="pending"

	while [ "$installation" != "finish" ] ; do
		installation="finish"
		pacman -Sy $args $packages || {
			
			read -p "The installation failed. Do you want to try again? [y/N]: " choice
			[ "$choice" != "y" ] && exit 1
			installation="failed"
			args="--needed"
		}
	done

	# Cleanup pacman cache, needed to fit the OS on a 8GB partition
	rm -rf /var/cache/pacman/pkg/*

done


# Fix theming and improve touchpad on firefox
echo -e "QT_STYLE_OVERRIDE=\"kvantum\"\nMOZ_USE_XINPUT2=1\nDXVK_ASYNC=1" >> /etc/environment


# Add mdns support to get wireless printers working
sed -i '/hosts/s/$/\ mdns/' /etc/nsswitch.conf


# Set default x.org keyboard layout
echo -e "Section \"InputClass\"\n\tIdentifier \"keyboard default\"\n\tMatchIsKeyboard \"yes\"\n\tOption \"XkbLayout\" \"$keyboard_layout\"\n\tOption \"XkbVariant\" \"$keyboard_variant\"\nEndSection" > /etc/X11/xorg.conf.d/10-keyboard.conf


# Configure lightdm gtk greeter
echo -e "[greeter]\ntheme-name = Arc-Dark\nicon-theme-name = Papirus-Dark\nfont-name = Noto Sans 10\nxft-antialias = true\nxft-dpi = 96\nxft-rgba = rgb\nxft-hintstyle = hintfull\nbackground = /usr/share/backgrounds/xfce/xfce-shapes.svg\nindicators = ~session;~language;~spacer;~a11y;~power" > /etc/lightdm/lightdm-gtk-greeter.conf


# Autologin with lightdm
[ "$autologin" == "y" ] && { 
	groupadd -r autologin
	gpasswd -a "$user" autologin
	sed -i -e '/autologin-user=/s/#//g' -e '/autologin-user=/s/=.*/='$user'/g' /etc/lightdm/lightdm.conf
}


# Disable Watchdog Timer and disable pc speaker
echo -e "blacklist iTCO_wdt\nblacklist pcspkr" > /etc/modprobe.d/blacklist.conf


# Add user to group games for improving performance with wine-ge and proton-ge
gpasswd -a "$user" games


# Configure Pipewire
# Create necessary folders if needed
test -d /etc/pipewire/ || mkdir /etc/pipewire/
test -d /etc/pipewire/filter-chain/ || mkdir /etc/pipewire/filter-chain/
test -d /etc/wireplumber/ || mkdir /etc/wireplumber/

# /etc/pipewire/minimal.conf
sed -e '/nice.level/s/=.*/=\ -15/g' -e '/audio.format/s/=.*/=\ \"S16\"/g' -e '/audio.format/s/#//g' -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/minimal.conf > /etc/pipewire/minimal.conf

# /etc/pipewire/client.conf
sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/client.conf > /etc/pipewire/client.conf

# /etc/pipewire/client-rt.conf
sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' /usr/share/pipewire/client-rt.conf > /etc/pipewire/client-rt.conf

# /etc/pipewire/pipewire.conf
sed -e '/nice.level/s/=.*/=\ -15/g' /usr/share/pipewire/pipewire.conf > /etc/pipewire/pipewire.conf

# /etc/pipewire/pipewire-pulse.conf
sed -e '/resample.quality/s/=.*/=\ '$audio_quality'/g' -e '/resample.quality/s/#//g' -e '/nice.level/s/=.*/=\ -15/g' -e '/pulse.default.format/s/=.*/=\ S16/g' -e '/pulse.default.format/s/#//g' -e '/module-switch-on-connect/s/#//g' /usr/share/pipewire/pipewire-pulse.conf > /etc/pipewire/pipewire-pulse.conf

# /etc/pipewire/filter-chain/demonic.conf
sed -e '/audio.format/s/=.*/=\ \"S16\"/g' -e '/audio.format/s/#//g' /usr/share/pipewire/filter-chain/demonic.conf > /etc/pipewire/filter-chain/demonic.conf

# /etc/wireplumber/wireplumber.conf
sed -e '/nice.level/s/=.*/=\ -15/g' /usr/share/wireplumber/wireplumber.conf > /etc/wireplumber/wireplumber.conf 

# /etc/wireplumber/bluetooth.conf
sed -e '/nice.level/s/=.*/=\ -15/g' /usr/share/wireplumber/bluetooth.conf > /etc/wireplumber/bluetooth.conf


# Disable bluetooth at boot
sed -i -e '/AutoEnable=/s/#//g' -e '/AutoEnable=/s/=.*/=false/g' /etc/bluetooth/main.conf

# Setup services
systemctl enable NetworkManager cups lightdm bluetooth ufw avahi-daemon

clear
