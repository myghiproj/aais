# Myghi63 Arch Advanced Install Scripts (DEPRECATED)

Please notice that all my effort will be put on the new [PowerArch Installer](https://gitlab.com/myghiproj/powerarch-installer). The new installer will have a lot of improvements over AAIS and I have to focus on it. You can still install the OS by using AAIS while PowerArch is not finished. 

<img src="https://i.imgur.com/olyDnhz.png" />

This project installs a very fast, optimized and pre-configured Arch Linux OS.

# Features

Supports booting via UEFI (without secure boot) and Legacy boot

Interactive installer, which asks you the required information and checks if you supplied a invalid one

Linux-zen kernel: Better multi-tasking performance and fsync patch for steam proton

BTRFS filesystem with zstd:1 compression and other optimizations: Timeshift support, great performance and less space usage

Full disk encryption support with luks1 (optional)

ZRAM with zstd compression: more swap (RAM/2) that brings good performance and doesn't hurt your HD/SSD

Swap behavior optimized to use with zram + lzo compression and earlyoom installed to avoid lock-ups in out-of-memory conditions

More minor optimizations

# Specific features for "desktop" mode

XFCE desktop environment pre-configured without unnecessary packages

Auto-login can be configured at installation proccess

Papirus icon theme and Arc-Dark theme applied for both QT and GTK applications (QT uses Kvantum!)

Full support for Unicode, CJK and Emoji fonts with Noto Fonts

Pipewire audio server installed and pre-configured to get very low latency and great quality, replacing pulseaudio too!

Advanced Power Management with TLP and udev rules to use less power on your machine, increasing battery life!

Supports Bluetooth, Network Printers, Wifi, open-source Vulkan, OpenGL, Vaapi and VDPAU drivers for Intel/AMD out of the box!

Few applications that are userful for everyone

# Complementary project to use with

I created another script to install and configure extra applications for my use.

If you want to use it, please check [this script](https://gitlab.com/myghiproj/ahss/-/blob/main/Bash%20Scripts/AAIS-Custom-Packages-and-Settings.sh) and change what you need!

# Hardware Requirements for both desktop and server modes

CPU: Any x86 64-bit capable processor

GPU: Anything that linux could support

RAM: At least 1GB

Storage: At least 8GB

# Disclaimer

You should know how to use Arch Linux before trying this script. If you don't know, ask someone for help!

Despite the fact that these scripts are made in a way that avoids data loss, they won't come with any warranty and I'm not responsible for any data loss!

# Note for nVidia users

If you have a nVidia GPU, you will have to install the proper drivers after the installation!

You also have to use the -dkms version of the driver to be compatible with linux-zen kernel.

Instead of installing the package "nvidia", install "nvidia-dkms"

Also don't forget to install "lib32-nvidia-utils" for 32-bit graphical applications!

# Preparation for dual boot

If you wanna install Arch Linux alongside your current OS, you need to resize the big main partition and create a empty one.

This will be used as a ROOT partition for the new OS.

You will need to indicate which one you created when you run the main 0-aais.sh script.

# Download and boot Arch Linux ISO

Please download the latest [ArchISO](https://archlinux.org/download/) from the website and use something like [Ventoy](https://www.ventoy.net/en/index.html) to make it bootable for your computer.

After that, boot the Arch Linux ISO and set your keyboard layout. For example, with a brazilian abnt2 keyboard:

```
# loadkeys br-abnt2
```

# Connect to the internet

If you have a wired network connection, you should already have internet connection to install Arch Linux.

If you need to connect to a Wi-Fi network, you can use iwd to connect:

```
# iwctl
[iwd]# device list
[iwd]# station [device name] get-networks
[iwd]# station [device name] connect [network name]
[iwd]# exit
```

With "device list" command, you should get something like wlan0 avaliable to use. If you don't, check if your wireless adapter is disabled:

```
# rfkill list
```

If it says "Soft blocked: yes", then run:

```
# rfkill unblock wifi
```

Then you should be able to connect using iwd.

If doesn't, it means that you have a wireless card with some obscure driver, like Broadcom. In this case, you probably need to use a cable or a wifi dongle instead.

# Install the new OS

To install using the scripts, type on the terminal:

```
bash <(curl -fsSL bit.ly/0aais)
```

After that, supply the needed informations and install it!

# Reporting issues

If you have any problem while using this project, please do a detailed report and tell which commit you have used to install.
