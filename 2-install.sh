#!/bin/bash

# Install basic system
# Copyright (C) 2022 - Myghi63 (contato-myghi63@protonmail.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

echo -e "2-install.sh Started\n"

root_part="$1"
region="$2"

# Essential packages
packages="base bash-completion linux-zen linux-firmware linux-tools smartmontools grub zram-generator power-profiles-daemon hdparm thermald irqbalance earlyoom sudo cronie rtkit nfs-utils ntfs-3g exfat-utils dosfstools btrfs-progs wireless-regdb ethtool iucode-tool intel-ucode amd-ucode nano lvm2"
[ "$root_part" == "false" ] || packages="$packages os-prober"
[ -d /sys/firmware/efi ] && packages="$packages efibootmgr"

# Setup and update time
timedatectl set-timezone "$region" ||:
timedatectl set-ntp true ||:

# Find the best mirrors to download packages"
while [ "$rankmirrors" != "finish" ] ; do
	rankmirrors="finish"
	echo -e "Do you want to select a specific country for the mirrors? Example: Brazil\nOnly recommended if you're using limited internet!"
	read -p "Your Choice (leave blank to ignore): " country
	country="$(echo $country | sed 's/\ //g')"
	if [ -z "$country" ] ; then
		echo -e "Now looking for the best servers to download packages! Please Wait, this can take a long time..."
	else
		rankmirrors_args="--country $country"
		echo -e "Now looking for the best servers to download packages! Please Wait..."
	fi
	
	reflector $rankmirrors_args --protocol https --age 12 --fastest 5 --sort rate --save=/etc/pacman.d/mirrorlist &> /dev/null || {
		echo ""
		read -p "Failed to rank mirrors. Do you want to try again? [y/N]: " choice
		[ "$choice" != "y" ] && exit 1
		rankmirrors="failed"
		rankmirrors_args=""
		echo ""
	}
done

# Avoid an problematic brazilian mirror that returns error 403
sed -i '/br.mirror.ar/s/^/#/' /etc/pacman.d/mirrorlist
		
# Enable parallel downloads
sed -i -e '/Parall/s/^#//' /etc/pacman.conf

# Update keyring and install basic system
while [ "$installation" != "finish" ] ; do
	installation="finish"
	pacman -Sy archlinux-keyring --noconfirm --needed && \
	pacstrap /mnt $packages || {
		read -p "The installation failed. Do you want to try again? [y/N]: " choice
		[ "$choice" != "y" ] && exit 1
		installation="failed"
	}
done

genfstab -U /mnt >> /mnt/etc/fstab 

clear
